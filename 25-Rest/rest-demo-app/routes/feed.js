const router = require('express').Router();
const feedCntr = require('../controllers/feed');

router.get('/posts', feedCntr.getPosts);
router.post('/post', feedCntr.createPost);

module.exports = router;
