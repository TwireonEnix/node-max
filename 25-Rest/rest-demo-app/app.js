const express = require('express');
const app = express();
const feedRoutes = require('./routes/feed');

app.use(express.json());
app.use(require('./middleware/cors'));
app.use(require('morgan')('dev'));
app.use('/feed', feedRoutes);

app.listen(3000, () => console.log(`Server running on 3000`));
